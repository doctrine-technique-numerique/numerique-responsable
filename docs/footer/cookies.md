# Politique de cookies

Utilisation de cookies strictement nécessaires ne nécessitant pas le consentement préalable. Il s’agit des cookies exclusivement déposés par l’éditeur du site. Ils sont indispensables à la navigation sur le site.