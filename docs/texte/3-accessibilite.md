# 3. Accessibilité numérique

## 3.1. Définition et périmètre

L’accessibilité numérique consiste à rendre les contenus et services numériques compréhensibles et utilisables par les personnes en situation de handicap.

## 3.2.	Principes généraux d’accessibilité

Depuis 2012, tous les sites publics doivent être accessibles et conformes à l’ensemble des critères du [référentiel général d’amélioration de l’accessibilité (RGAA)](https://accessibilite.numerique.gouv.fr/ "Lien vers référentiel général d’amélioration de l’accessibilité (RGAA)") pour ainsi permettre à tous les usagers un égal accès à leurs droits.

## 3.3. Textes et outils de référence

Les services publics numériques et certains services privés ont l’obligation d’être accessibles de façon équivalente à tout citoyen, qu’il soit ou non en situation de handicap. Un service numérique accessible est plus facile à utiliser pour les personnes handicapées et de meilleure qualité pour tous.

Afin de faciliter la mise en accessibilité des sites et services numériques, la direction interministérielle du numérique (Dinum) édite le référentiel général d’amélioration de l’accessibilité. Il contribue à la mise en œuvre de l’article 47 de la [loi n° 2005-102 du 11 février 2005 pour l'égalité des droits et des chances, la participation et la citoyenneté des personnes handicapées](https://www.legifrance.gouv.fr/loda/id/JORFTEXT000000809647 "Lien vers la loi") et son [décret d’application n° 2019-768 du 24 juillet 2019 relatif à l'accessibilité aux personnes handicapées des services de communication au public en ligne](https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000038811937 "Lien vers le décret").

Le volet technique du RGAA est une méthode d’application de la norme internationale (une norme internationale émise par le W3C (World Wide Web Consortium) : [WCAG 2.1](https://www.w3.org/TR/WCAG21/ "Lien vers le site du W3C) ; une norme européenne émise par le ETSI (European Telecommunications Standards Institute) : [EN 301 549 v3.2.1 (2021-03)](https://eur-lex.europa.eu/legal-content/FR/TXT/HTML/?uri=CELEX:32021D1339 "Lien vers le texte de la norme")).

Pour assurer la conformité d’un service numérique au RGAA, il convient de produire les éléments suivant dans chaque service en ligne :

* La **déclaration d’accessibilité** qui doit contenir les informations suivantes :
    * un état de conformité ; 
    * un signalement des contenus non accessibles ;
    * des dispositifs d’assistance et de contact ;
    * la mention de la faculté pour la personne concernée de saisir le Défenseur des droits.
* Afficher sur **toutes les pages** le taux de conformité au RGAA du site ou service :
    * « Accessibilité : non conforme », si aucun audit n’a été effectué ou si le résultat est inférieur à 50 % ;
    * « Accessibilité : partiellement conforme » si le résultat de l’audit est supérieur à 50 % ;
    * « Accessibilité : totalement conforme » si le taux est égal à 100 %.
* **En plus de l’obligation de conformité totale au RGAA**, chaque service en ligne doit proposer un lien vers le **schéma pluriannuel de mise en accessibilité numérique de l’entité** et le plan d’action de l’année en cours. Le schéma pluriannuel de mise en accessibilité et le plan d’action sont rédigés par le **référent accessibilité numérique**. Le schéma pluriannuel de mise en accessibilité détaille la mise en œuvre de l’accessibilité dans une organisation, elle se fait généralement en quatre étapes :
    * évaluation : identification des services et évaluation de leur niveau d’accessibilité ;
    * consignation : production d’une déclaration d’accessibilité par service ;
    * organisation : mise en place d’une organisation pour améliorer les services et définition des plans d’actions annuels ;
    * communication de ces éléments sur le site de l’organisation.
* Tous les sites et applications utilisées au travers d’un navigateur web sont concernés :
    * les sites internet, intranet, extranet et progiciels ;
    * les applications mobiles ;
    * le « mobilier numérique ».

Le RGAA fait régulièrement l’objet de nouvelles versions et mises à jour pour s’adapter aux évolutions du web mais aussi aux changements de normes et réglementations.

## 3.4. Sanctions

Le manquement aux obligations déclaratives décrites ci-dessus peut entraîner une sanction financière prononcée par l’Arcom[^1].

[^1]:  Source : https://www.arcom.fr/nous-connaitre/nos-missions/garantir-le-pluralisme-et-la-cohesion-sociale/les-droits-des-personnes-handicapees/accessibilite-des-sites-et-des-services-numeriques

En cas de manquements relevés par des agents assermentés, l’Arcom pourra mettre en demeure les personnes morales dont dépend le service de se conformer aux dispositions légales. Si celles-ci persistaient à ne pas se conformer à la loi, alors l’Autorité pourrait prononcer à leur encontre des sanctions pécuniaires, modulables en fonction de la nature, de la gravité et de la durée du manquement.

## 3.5.	Axes à prendre en compte pour la mise en accessibilité

Comme cela est indiqué dans le RGAA, différents axes sont à prendre en compte pour s’inscrire dans un processus d'accessibilité qui intègre les contraintes techniques et les critères réglementaires à respecter.

![](Figures/Figure3.5.png)

## 3.6. Exigences pour les services numériques pour l'éducation

!!!regle "Exigence"
    Les services numériques pour l’éducation doivent être rendus accessibles conformément au RGAA. 
