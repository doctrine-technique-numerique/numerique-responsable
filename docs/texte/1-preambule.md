# 1. Préambule

## 1.1. Contexte

Ce document constitue le référentiel du numérique responsable pour l’éducation. Il s’inscrit dans le corpus des référentiels de la doctrine technique du numérique pour l’éducation, et dans la stratégie du numérique pour l’éducation qui rappelle l’ambition du ministère pour une réduction de la consommation énergétique globale de 10 % pour 2024 et la neutralité carbone en 2050 et l’engagement nécessaire de chaque fournisseur de services numériques pour tendre vers cet objectif commun.

Il recense les exigences et recommandations en matière d’écoresponsabilité et d’accessibilité numérique pour l’ensemble des fournisseurs de services numériques pour l’éducation.

Il s’appuie sur des documents de référence généraux comme le RGESN ou le RGAA, et contextualise les exigences au périmètre des services numériques pour l’éducation. Le ministère, ses fournisseurs de services ainsi que l’ensemble des partenaires et acteurs des services numériques ont participé à l’élaboration de ce référentiel. 

Dans le cadre de ce référentiel, il est entendu que le « numérique responsable » est un processus d’amélioration continue qui vise à réduire l’empreinte environnementale et sociale du numérique.

Sur le plan environnemental, il revendique une utilisation plus sobre et moins énergivore de l’outil numérique en faisant évoluer les pratiques, les usages et les projets de déploiement des équipements et services vers plus de sobriété, sans tomber dans l’austérité.

Sur le plan social, le numérique responsable vise à garantir l'inclusion numérique, l'accès équitable aux technologies et la protection des droits de l'homme. Cela inclut la lutte contre la fracture numérique et la promotion de l'éthique numérique.

Sur le plan économique, le numérique responsable encourage des modèles d'affaires durables, la transparence financière et la création d'emplois éthiques. Il s'agit de promouvoir une économie numérique qui contribue positivement au bien-être économique global.

## 1.2. Objectifs du document

Cette première version du référentiel aborde deux grands axes : l'écoresponsabilité et l'accessibilité numérique.

## 1.3.	Cycle de vie du document et gouvernance

Le ministère et ses services déconcentrés ainsi que les collectivités territoriales ont participé à l’élaboration de ce référentiel. 

Le présent document constitue la première version du référentiel du numérique responsable pour l’éducation.

Une mise à jour annuelle du document est prévue à l’instar de la doctrine technique du numérique pour l’éducation. 
