# 2. Éco-responsabilité numérique

## 2.1. Définition

L'écoconception d'un service numérique éducatif consiste à intégrer des contraintes environnementales dans tous les processus de développement, afin de réduire les impacts environnementaux du service numérique pendant son cycle de vie[^1].

[^1]: Luc, N. (2023, 30 janvier). L’éco-conception des produits. Ministères Écologie Énergie Territoires. https://www.ecologie.gouv.fr/leco-conception-des-produits 

## 2.2. Principes généraux d’éco-conception

Plus la prise en compte des aspects environnementaux intervient tôt dans le cycle de vie du service numérique (avant son développement, pendant ou après), plus l'effet est important.

L’intégration de quelques principes de base doit permettre de réduire la consommation de ressources informatiques et énergétiques des terminaux, des réseaux et gisements de données : 

* simplicité des fonctionnalités ;
* frugalité et sobriété de l’architecture applicative ;
* pertinence, soit : utilité / temps de réponse / accessibilité ;
* durabilité, en favorisant la réutilisation et l’ouverture des briques logicielles, ainsi que l’utilisation d’API pour faciliter les échanges de données.

La planification de l’utilisation de données par le service numérique, en suivant le principe *FAIR (Findable, Accessible, Interoperable, Reusable)*, soit faciles à trouver, accessibles, interopérables, réutilisables est une composante importante du processus[^3].

[^3]: Pasteur, C.-. I. (2023, août 2). Les principes FAIR : findable, accessible, interoperable, reusable. Open science : évolutions, enjeux et pratiques. https://openscience.pasteur.fr/2020/10/05/les-principes-fair-findable-accessible-interoperable-reusable/ 

Afin de répondre de manière efficace aux enjeux cités, il est nécessaire d’explorer les recommandations officielles et parallèlement la mise en œuvre de méthodes et d’outils appropriés. 

## 2.3.	Textes et outils de référence

### [Le Référentiel Général d’Écoconception des Services Numériques (RGESN)](https://www.arcep.fr/mes-demarches-et-services/entreprises/fiches-pratiques/referentiel-general-ecoconception-services-numeriques.html "Lien vers le Référentiel Général d’Écoconception des Services Numériques (RGESN)") 

Il a été élaboré par l’Arcep et l’Arcom, en collaboraiton avec l’ADEME, la DINUM, la CNIL et l’Inria. Ce document s’appuie en particulier sur les travaux antérieurs de la mission interministérielle Numérique écoresponsable (MiNumEco) menée par la DINUM et l’ADEME, en collaboration avec le ministère de la Transition écologique et l’Institut du numérique responsable (RGESN novembre 2022).

### [La feuille de route gouvernementale « numérique et environnement »](https://www.gouvernement.fr/actualite/numerique-et-environnement-la-feuille-de-route-du-gouvernement "Lien vers la feuille de route gouvernementale « numérique et environnement »")

Elle est composée de trois piliers : 

* le développement de la connaissance de l’empreinte environnementale numérique pour agir efficacement ;
* le soutien au numérique plus sobre en réduisant l’empreinte environnementale du numérique ; 
* le numérique comme levier de la croissance écologique.

### [Rapport sur l’obsolescence logicielle du Conseil général de l’économie](https://www.economie.gouv.fr/cge/obsolescence-logicielle "Lien vers le rapport sur l’obsolescence logicielle du Conseil général de l’économie")

Il répertorie les différents textes nationaux et européens sur l’écoconception et l’obsolescence logicielle. Il permet de les consulter par fonctionnalité (obsolescence programmée, augmentation de la durée de fourniture des mises à jour, dissociations des mises à jour correctives/évolutives…).

### [Boite à outils du numérique écoresponsable](https://ecoresponsable.numerique.gouv.fr/publications/boite-outils/ "lien vers la boite à outils du numérique écoresponsable")

Une boite à outils est mise à disposition sur le site de la MiNumÉco. Elle propose une sélection (non-exhaustive) de logiciels (libres et open source) dédiés aux impacts environnementaux du numérique.
Cette boite à outils permet notamment de réaliser une autoévaluation de la démarche d’écoconception d’un service numérique, sur la base des 79 critères du RGESN.

## 2.4. Niveaux d'éco-conception

Comme cela est indiqué dans le RGESN, différents axes sont à prendre en compte pour s’inscrire dans une démarche d’écoconception des services numériques pour l’éducation :

![](Figures/Figure2.4.png)

## 2.5. Exigences pour les services numériques pour l’éducation

En complément du cadre général rappelé dans les paragraphes précédents, la liste ci-dessous identifie des critères d’exigences permettant d’intégrer de façon opérationnelle et concertée des actions pour l’écoconception dans le périmètre du numérique éducatif. Cette liste est évolutive et concertée avec les porteurs de projet.

!!!regle "Exigence"
    Les actions de développement des services numériques pour l’éducation doivent respecter un processus d’écoconception qui intègre les contraintes environnementales et les recommandations du RGESN. 

!!!regle "Exigence"
    Chaque fournisseur de service numérique éducatif publie une déclaration d’écoconception.
